import { useRouter } from 'next/router';
import { useMutation } from 'react-query';
import { useAtom } from 'jotai';
import toast from 'react-hot-toast';
import client from '@/data/client';
import usePrice from '@/lib/hooks/use-price';
import LoopIcon from '@mui/icons-material/Loop';
import Button from '@/components/ui/button';
import { useCart } from '@/components/cart/lib/cart.context';
import {
  calculatePaidTotal,
  calculateTotal,
  calculateTomxu,
} from '@/components/cart/lib/cart.utils';
import CartWallet from '@/components/cart/cart-wallet';
import { usePhoneInput } from '@/components/ui/forms/phone-input';
import {
  payableAmountAtom,
  useWalletPointsAtom,
  verifiedTokenAtom,
  checkoutAtom,
} from '@/components/cart/lib/checkout';
import PaymentGrid from '@/components/cart/payment/payment-grid';
import routes from '@/config/routes';
import { useTranslation } from 'next-i18next';
import { PaymentGateway } from '@/types';
import { useSettings } from '@/data/settings';
import { REVIEW_POPUP_MODAL_KEY } from '@/lib/constants';
import Cookies from 'js-cookie';
import { useEffect, useRef, useState } from 'react';
import { useMe } from '@/data/user';
import PhoneInput from '@/components/ui/forms/phone-input';
export default function CartCheckout(props: any) {
  const { settings } = useSettings();
  const router = useRouter();
  const { t } = useTranslation('common');
  const { me } = useMe();
  const [userTomxu, setUserTomxu] = useState();

  const { mutate, isLoading, data } = useMutation(client.orders.create, {
    onSuccess: (res) => {
      // props.showPay(true);
      return router.push(`${routes.orderUrl(res.tracking_number)}/payment`);
    },
    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });

  const [{ payment_gateway }] = useAtom(checkoutAtom);
  const [use_wallet_points] = useAtom(useWalletPointsAtom);
  const [payableAmount] = useAtom(payableAmountAtom);
  const [token] = useAtom(verifiedTokenAtom);
  const { items, verifiedResponse } = useCart();
  const [totalTomxu, setTotalTomxu] = useState();
  const addressRef = useRef<any>();
  const phoneRef = useRef<any>();
  const [addressError, setAddressError] = useState(false);

  const available_items = items.filter(
    (item) =>
      !verifiedResponse?.unavailable_products?.includes(item.id.toString()),
  );

  const base_amount = calculateTotal(available_items);

  useEffect(() => {
    const tomxu: any = calculateTomxu(items);
    if (tomxu) {
      setTotalTomxu(tomxu);
    }
  }, [items]);

  const totalPrice = verifiedResponse
    ? calculatePaidTotal(
        {
          totalAmount: base_amount,
          tax: verifiedResponse.total_tax,
          shipping_charge: verifiedResponse.shipping_charge,
        },
        0,
      )
    : 0;

  const { price: total } = usePrice(
    verifiedResponse && {
      amount: totalPrice,
    },
  );
  const { phoneNumber } = usePhoneInput();

  function createOrder() {
    if (!addressRef.current.value) {
      if(!addressRef.current.value) {
        setAddressError(true)
        return toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      }else {
        setAddressError(false)
      }
     
    }

    const isFullWalletPayment = false;
    let gateWay = isFullWalletPayment
      ? PaymentGateway.FULL_WALLET_PAYMENT
      : payment_gateway;

    mutate({
      amount: base_amount,
      total: totalPrice ?? 1,
      paid_total: totalPrice ?? 1,
      products: available_items.map((item) => ({
        product_id: item.id,
        order_quantity: item.quantity,
        unit_price: item.price,
        subtotal: item.price * item.quantity,
        tomxu: item.tomxu * item.quantity,
        tomxu_subtotal: item.tomxu * item.quantity,
      })),
      payment_gateway: PaymentGateway.CASH,
      total_tomxu: totalTomxu,
      use_wallet_points,
      isFullWalletPayment,
      ...(token && { token }),
      sales_tax: verifiedResponse?.total_tax ?? 0,
      customer_contact: phoneNumber ? phoneNumber : '1',
      address: addressRef.current.value,
    });
  }

  function rechargeHandler() {
    window.open(process.env.NEXT_PUBLIC_RECHARGE_URL, '_blank');
  }
  const { mutate: getTomxu } = useMutation(client.userTomxu.getTomxu, {
    onSuccess: (res) => {
      console.log('check res', res);
    },
    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });
  function refresh() {
    getTomxu({ customer_id: me?.id, type: 1 });
  }


  return (
    <div className="mt-10 border-t border-light-400 bg-light pt-6 pb-7 dark:border-dark-400 dark:bg-dark-250 sm:bottom-0 sm:mt-12 sm:pt-8 sm:pb-9">
      <div className="mb-6 flex flex-col gap-3 text-dark dark:text-light sm:mb-7">
        <div className="flex justify-between">
          <p className="mt-3">{t('text-address')}</p>
          <input
            className={`rounded-md w-2/3 text-black ${addressError ? 'border border-red-500 border-solid border-2' : ''}`}
            ref={addressRef}
            required
            type="text"
          ></input>
        </div>

        <div className="flex justify-between mt-5">
          <p>{t('text-subtotal')}</p>
          <strong className="font-semibold pr-7">{`${totalTomxu} Tomxu`}</strong>
        </div>

        <div className="flex justify-between border-t border-light-400 bg-light pt-6 pb-7 dark:border-dark-400 dark:bg-dark-250 sm:bottom-0 sm:mt-12 sm:pt-8 sm:pb-9">
          <p>{t('Số dư ví hiện tại')}</p>
          <div className="flex ">
            {userTomxu && (
              <strong className="font-semibold pr-7">{`${userTomxu} Tomxu`}</strong>
            )}

            {/* <img
              src="icons/arr.png"
              alt="img"
              className="w-9 hover:scale-110 "
            /> */}
            <div onClick={refresh} className="hover:scale-110">
              <LoopIcon />
            </div>
          </div>
        </div>

        <button
          className="w-2/6 md:h-[50px] md:text-sm bg-[#F91111] ml-auto hover:bg-red-300 text-white rounded-md"
          onClick={rechargeHandler}
        >
          {t('Nạp tiền')}
        </button>
      </div>
      <Button
        disabled={isLoading}
        isLoading={isLoading}
        onClick={createOrder}
        className="w-full md:h-[50px] md:text-sm bg-[#24b47e] ml-auto hover:bg-[#2f9e44] text-white rounded-md"
      >
        {t('text-submit-order')}
      </Button>
    </div>
  );
}
