import { useMutation } from 'react-query';
import { useTranslation } from 'next-i18next';
import Button from '@/components/ui/button';
import client from '@/data/client';
import { useCart } from '@/components/cart/lib/cart.context';
import toast from 'react-hot-toast';
import { useRouter } from 'next/router';
import { useRef, useState, useEffect } from 'react';
import { useOrderPayment, useOrder } from '@/data/order';
import ThankYou from '@/pages/orders/[tracking_number]/thank-you';
const OrderOTP = (props: any) => {
  const { items } = useCart();
  const { t } = useTranslation('common');
  const router = useRouter();
  const OPTRef = useRef<any>(null);
  const [seconds, setSeconds] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [thank, setThank] = useState(false);
  useEffect(() => {
    let intervalId: any;
    if (isActive) {
      intervalId = setInterval(() => {
        setSeconds((prevSeconds) => {
          if (prevSeconds === 0) {
            clearInterval(intervalId);
            setIsActive(false);
            return 0;
          }
          return prevSeconds - 1;
        });
      }, 1000);
    }
    return () => clearInterval(intervalId);
  }, [isActive]);
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;

  const { mutate, isLoading, data } = useMutation(client.payment.post, {
    onSuccess: (res) => {
      console.log('check', res);

      toast.success(<b>{t('payment-success')}</b>);
      setThank(true);
      setIsActive(false);
      setTimeout(() => {
        // offTable();
        setThank(false);
      }, 15000);
    },

    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });

  function offTable() {
    props.showOTP(false);
    setThank(false);
  }

  function paymentHandler() {
    if (OPTRef.current.value.length === 0) {
      return toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    }

    const sendData = {
      customer_id: props.order.customer.id,
      customer_name: props.order.customer.name,
      total_tomxu: props.order.total_tomxu,
      customer_contact: props.order.customer_contact,
      otp: OPTRef.current.value,
      type: 13,
      tracking_number: props.order.tracking_number,
      products: props.order.products.map((item: any) => ({
        product_id: item.id,
        quantity: Number(item.pivot.order_quantity),
        tomxu: item.tomxu.price_tomxu,
        tomxu_subtotal: item.tomxu.price_tomxu * item.pivot.order_quantity,
        shop_id: item.shop_id,
      })),
    };
    console.log(sendData);
    // 418556
    mutate(sendData);
  }

  const { mutate: sendOTP, isLoading: ld } = useMutation(
    client.payment.sendOTP,
    {
      onSuccess: () => {
        setIsActive(true);
        setSeconds(120);
      },
      onError: (error: any) => {
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );

  function verify() {
    const sendData = {
      used_id: props.order.customer.id,
      email: props.order.customer.email,
      type: 'verify_order',
    };

    sendOTP(sendData);
  }

  function refresh() {
    if (seconds === 0) {
      verify();
    }
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 top-0 bg-black bg-opacity-50 ">
      {!thank && (
        <div className="relative w-96 h-auto mx-auto max-w-screen-sm flex-col p-6 pt-6 sm:p-5 sm:pt-8 md:pt-10 3xl:pt-12 bg-light shadow-card dark:bg-dark-250 dark:shadow-none text-center flex items-center justify-center rounded ">
          <button onClick={offTable} className="absolute top-0 right-0 p-4 ">
            X
          </button>
          <div className="mb-8 leading-tight">
            <h2 className="text-center  text-lg mb-10">Vui lòng nhập OTP </h2>

            <p>Mã OTP của bạn được gửi về</p>
            <p>{props.email}</p>

            <div>
              <input
                className="rounded mt-5"
                ref={OPTRef}
                placeholder="nhập OTP"
              ></input>
              {isActive && (
                <p className="mt-5 text-red-400">
                  {minutes}:{remainingSeconds < 10 ? '0' : ''}
                  {remainingSeconds}
                </p>
              )}
              {ld && (
                <p className="text-red-400 mt-5">
                  Mã OTP của bạn đang được gửi
                </p>
              )}
            </div>
            <div>
              <button
                className={`mt-5 ${isActive ? 'pointer-events-none' : ''}`}
                onClick={refresh}
              >
                Gửi mã
              </button>
            </div>
          </div>

          <div className="flex">
            <Button
              className="mr-10 "
              onClick={paymentHandler}
              isLoading={isLoading}
            >
              {t('text-submit-confirm')}
            </Button>
            <Button className="bg-red-500 hover:bg-red-300" onClick={offTable}>
              {t('text-cancel')}
            </Button>
          </div>
        </div>
      )}
      {thank && <div onClick={offTable}>{<ThankYou />}</div>}
    </div>
  );
};
export default OrderOTP;
