// import lightModeLogo from '@/assets/images/logo-black.svg';
// import darkModeLogo from '@/assets/images/logo-white.svg';
import lightModeLogo from 'public/icons/tomiru.png';
import darkModeLogo from 'public/icons/tomiru.png';
export const siteSettings = {
  lightLogo: lightModeLogo,
  darkLogo: darkModeLogo,
  width: 128,
  height: 40,
};
