import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dayjs from 'dayjs';
import { GetServerSideProps } from 'next';
import isEmpty from 'lodash/isEmpty';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import ReactConfetti from 'react-confetti';
import type { NextPageWithLayout } from '@/types';
import GeneralLayout from '@/layouts/_general-layout';
import { useWindowSize } from '@/lib/hooks/use-window-size';
import { useCart } from '@/components/cart/lib/cart.context';
import { useTranslation } from 'next-i18next';
import { dehydrate, QueryClient } from 'react-query';
import { API_ENDPOINTS } from '@/data/client/endpoints';
import client from '@/data/client';
import type { SettingsQueryOptions } from '@/types';
import usePrice from '@/lib/hooks/use-price';
import OrderViewHeader from '@/components/orders/order-view-header';
import OrderStatusProgressBox from '@/components/orders/order-status-progress-box';
import { OrderStatus, PaymentStatus } from '@/types';
import { formatString } from '@/lib/format-string';
import { OrderItems } from '@/components/orders/order-items';
import { CheckMark } from '@/components/icons/checkmark';
import SuborderItems from '@/components/orders/suborder-items';
import { useOrder } from '@/data/order';
import { useModalAction } from '@/components/modal-views/context';
import { PageLoader } from '@/components/ui/loader/spinner/spinner';
import { Order } from '@/types';
import ErrorMessage from '@/components/ui/error-message';
import { getOrderPaymentSummery } from '@/lib/get-order-payment-summery';
import { calculateTomxu } from '@/components/cart/lib/cart.utils';
import OrderOTP from '@/components/orders/order-OTP';
import ThankYou from '@/pages/orders/[tracking_number]/thank-you';
type Props = {
  title: string;
  details: string | undefined;
};

const Card = ({ title, details }: Props) => {
  return (
    <div className="flex min-h-[6.5rem] items-center rounded border border-gray-200 py-4 px-6 dark:border-[#434343] dark:bg-dark-200">
      <div>
        <h3 className="mb-2 text-xs font-normal dark:text-white/60">
          {title} :{' '}
        </h3>
        <p className="text-dark-200 dark:text-white">{details}</p>
      </div>
    </div>
  );
};

const Listitem = ({ title, details }: Props) => {
  return (
    <p className="text-body-dark mt-5 flex items-center text-xs">
      <strong className="w-5/12 sm:w-4/12">{title}</strong>
      <span>:</span>
      <span className="w-7/12 ltr:pl-4 rtl:pr-4 dark:text-white sm:w-8/12 ">
        {details}
      </span>
    </p>
  );
};

const AddressOrder = ({title, details} : Props) => {
  return(
    <p className='text-body-dark mt-5 ml-20 text-xs pl-3'>   
    <strong className='w-5/12 sm:w-4/12 mr-10'>{title} </strong>
    <span>:</span>
    <span className="w-7/12 ltr:pl-4 rtl:pr-4 dark:text-white sm:w-8/12 ">
        {details}
      </span>
    </p>
  )
}

const InfoOrder = ({title, details} : Props) => {
  return (
    <p className="text-body-dark mt-5 flex items-center text-xs">
      <strong className="w-5/12 sm:w-4/12">{title}</strong>
      <span>:</span>
      <span className="w-7/12 ltr:pl-4 rtl:pr-4 dark:text-white sm:w-8/12 ">
        {details}
      </span>
    </p>
  );
}

interface OrderViewProps {
  order: Order | undefined;
  loadingStatus?: boolean;
}

const OrderView = ({ order, loadingStatus }: OrderViewProps) => {
  const [showOTP, setShowOTP] = useState(false);
  const { t } = useTranslation('common');
  const { width, height } = useWindowSize();
  const { resetCart } = useCart();

  useEffect(() => {
    resetCart();
  }, []);
  console.log(order)

  const { is_payment_gateway_use, is_full_paid, amount_due, gateway_payment } =
    getOrderPaymentSummery(order!);

  return (
    <div className="p-4 sm:p-8">
      {showOTP && (
        <OrderOTP
          showOTP={setShowOTP}
          id={order?.tracking_number}
          email={order?.customer.email}
          order={order}
        />
      )}

      <div className="mx-auto w-full max-w-screen-lg ">
        <div className="relative overflow-hidden rounded">
          <OrderViewHeader
            order={order}
            buttonSize="small"
            loading={loadingStatus}
            show={setShowOTP}
          />


          <div className="bg-light px-6 pb-12 pt-9 dark:bg-dark-200 lg:px-8">
            <div className="mb-6 grid gap-4 sm:grid-cols-2 md:mb-12 lg:grid-cols-4">
              <Card
                title={t('text-order-number')}
                details={order?.tracking_number}
              />
              <Card
                title={t('text-date')}
                details={dayjs(order?.created_at).format('DD-MM-YYYY')}
              />
              <Card
                title={t('text-total')}
                details={`${order?.total_tomxu} Tomxu`}
              />
              <Card
                title={t('text-payment-method')}
                // details={order?.payment_gateway ?? 'N/A'}
                details={'TOMXU'}
              />
            </div>

          <div className="mt-12 flex flex-row md:flex-row ml-20">
                <h2 className="mb-3 text-base font-medium dark:text-white ml-3">
                  {t('text-order-information')} 
                </h2>
          </div>
          <div className="flex flex-row md:flex-row ml-20">
            <div className="w-full md:w-1/2 ltr:md:pl-3 rtl:md:pr-3">
                  <InfoOrder
                    title={t('text-name-user')}
                    details={`${order?.customer_name}`}
                  />
              {/* <div className="border-b border-solid border-gray-200 py-1 dark:border-b-[#434343]" />    */}
            </div>
            <div className="w-full md:w-1/2 ltr:md:pl-3 rtl:md:pr-3 ml-20">
                  <InfoOrder
                    title={t('text-phone-number')}
                    details={`${order?.customer_contact}`}
                  />
              {/* <div className=" w-2/3 border-b border-solid border-gray-200 py-1 dark:border-b-[#434343]" /> */}
            </div>
          </div>
          <div className='w-full mt-5'>
          <AddressOrder
                    title={t('text-address')}
                    details={`Ngõ 100/100, Đồng Me, Mễ Trị Thượng, Nam Từ Liêm, Hà Nội`}
                  />
          </div>
      
          <div className="mt-7 w-full border-b border-solid border-[#dbdbdbd]-200 py-1 dark:border-b-[#434343]" />

          {/* end of info's user and order */}

            <div className="mt-7 flex flex-row md:flex-row">
              <div className="w-full md:w-1/2 ltr:md:pl-3 rtl:md:pr-3 ml-20">
                <h2 className="mb-6 text-base font-medium dark:text-white">
                  {t('text-order-status')}
                </h2>
                <div>
                  <OrderStatusProgressBox
                    orderStatus={order?.order_status as OrderStatus}
                    paymentStatus={order?.payment_status as PaymentStatus}
                  />
                </div>
              </div>
              {/* end of order details */}


              <div className="w-full md:w-1/2 ltr:md:pl-3 rtl:md:pr-3 ml-20">
                <h2 className="mb-6 text-base font-medium dark:text-white">
                  {t('text-order-details')}
                </h2>
                <div>
                  <Listitem
                    title={t('text-total-item')}
                    details={`${order?.products?.length}
                    ${t('text-item')}`}
                  />
                  <div className="w-1/2 border-b border-solid border-gray-200 py-1 dark:border-b-[#434343]" />
                  <Listitem
                    title={t('text-total')}
                    details={`${order?.total_tomxu} Tomxu`}
                  />
                </div>
              </div>

              {/* end of total amount */}
            </div>
            <div className="mt-12">
              <OrderItems
                products={order?.products}
                orderId={order?.id}
                status={order?.payment_status as PaymentStatus}
              />
            </div>
          </div>
        </div>
      </div>
      {order && order.payment_status === PaymentStatus.SUCCESS ? (
        <ReactConfetti
          width={width - 10}
          height={height}
          recycle={false}
          tweenDuration={8000}
          numberOfPieces={300}
        />
      ) : (
        ''
      )}
    </div>
  );
};

const OrderPage: NextPageWithLayout = () => {
  const { query } = useRouter();
  const { openModal } = useModalAction();
  const { order, isLoading, error, isFetching } = useOrder({
    tracking_number: query.tracking_number!.toString(),
  });

  const { payment_status, payment_intent, tracking_number } = order ?? {};

  useEffect(() => {
    if (
      payment_status === PaymentStatus.PENDING &&
      payment_intent?.payment_intent_info &&
      !payment_intent?.payment_intent_info?.is_redirect
    ) {
      openModal('PAYMENT_MODAL', {
        paymentGateway: payment_intent?.payment_gateway,
        paymentIntentInfo: payment_intent?.payment_intent_info,
        trackingNumber: tracking_number,
      });
    }
  }, [payment_status, payment_intent?.payment_intent_info]);

  if (isLoading) {
    return <PageLoader showText={false} />;
  }

  if (error) return <ErrorMessage message={error?.message} />;

  return <OrderView order={order} loadingStatus={!isLoading && isFetching} />;
};

OrderPage.authorization = true;
OrderPage.getLayout = function getLayout(page: any) {
  return <GeneralLayout>{page}</GeneralLayout>;
};

export default OrderPage;

export const getServerSideProps: GetServerSideProps = async ({ locale }) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    [API_ENDPOINTS.SETTINGS, { language: locale }],
    ({ queryKey }) => client.settings.all(queryKey[1] as SettingsQueryOptions),
  );

  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common'])),
      dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient))),
    },
  };
};
